﻿
namespace GestionBanque_06.Classes
{
    //Définir la classe « Compte » comme étant abstraite.

    public abstract class Compte
    {
        public Personne Titulaire { get; set; }
        private double _solde;
        private double _ligneDeCredit;
        public string Numero { get; set; }

        public virtual double LigneDeCredit
        {
            get
            {
                return _ligneDeCredit;
            }
            set
            {
                if (value >= 0)
                {
                    _ligneDeCredit = value;
                }
            }
        }

        public virtual double Solde
        {
            get
            {
                return _solde;
            }
            set
            {
                if (value >= -LigneDeCredit)
                {
                    _solde = value;
                }
            }
        }

        public static double operator +(double a, Compte b)
        {
            
            return a + ((b.Solde > 0) ? b.Solde : 0);
        }


        public virtual void Retrait(double montant)
        {
            if (montant <= 0)
            {
                return;
                /*to do: remplacer par une erreur*/
            }
            Solde = Solde - montant;
        }
        public void Depot(double montant)
        {
            if (montant <= 0)
            {
                return;
                /*to do: remplacer par une erreur*/
            }
            Solde = Solde + montant;
        }

        // Ajouter une méthode abstraite « protected » à la classe « Compte » appelée « CalculInteret » ayant pour prototype « double CalculInteret() » en sachant que pour un livret d’épargne le taux est toujours de 4.5% tandis que pour le compte courant si le solde est positif le taux sera de 3% sinon de 9.75%.
        protected abstract double CalculInteret();

        // 3. Ajouter une méthode « public » à la classe « Compte » appelée « AppliquerInteret » qui additionnera le solde avec le retour de la méthode « CalculInteret »
        public void AppliquerInteret()
        {
            Solde += CalculInteret();
        }

    }
}

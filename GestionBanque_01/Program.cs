﻿using GestionBanque_01.Class;

Personne Bruno = new Personne();
Bruno.Nom = "Step";
Bruno.Prenom = "Bruno";
Bruno.DateNaiss = new DateTime(2000, 01, 01);

Courant Compte = new Courant();
Compte.Numero = "01234567BEFROSEF";
Compte.LigneDeCredit = 500;
Compte.Titulaire = Bruno;

bool continuer = true;
do
{
    Console.Clear();
    Console.WriteLine("Voici votre solde : " + Compte.Solde);
    Console.WriteLine("Que souhaitez-vous faire ?");
    Console.WriteLine("1) Retirer");
    Console.WriteLine("2) Déposer");
    Console.WriteLine("3) Quitter");

    switch (Console.ReadLine())
    {
        case "1":
            Console.WriteLine("Veuillez entrer le montant à retirer ");
            if (double.TryParse(Console.ReadLine(), out double montantRetrait))
            {
                Compte.Retrait(montantRetrait);
            }
            break;

        case "2":
            Console.WriteLine("Veuillez entrer le montant à déposer ");

            if (double.TryParse(Console.ReadLine(), out double montantDepot))
            {
                Compte.Depot(montantDepot);
            }
            break;

        case "3":
            continuer = false;
            break;

        default:
            Console.WriteLine("Commande inconnue !");
            break;
    }


} while (continuer);





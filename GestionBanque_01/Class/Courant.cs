﻿
namespace GestionBanque_01.Class
{
    public class Courant
    {
        private double _ligneDeCredit;
        private double _solde;
        public string Numero {  get; set; }
        public double Solde { 
            get 
            {
                return _solde;
            } 
            private set 
            {
                if (value < -LigneDeCredit)
                {
                    return;
                    /*remplacer par une erreur*/
                }
                else
                {
                    _solde = value;
                }
            } 
        }
        public double LigneDeCredit {             
            get 
            { 
                return _ligneDeCredit; 
            } 
            set 
            {
                if (value >= 0) {
                    _ligneDeCredit = value;
                }
            } 
        }
        public Personne Titulaire { get; set; }

        public void Retrait(double montant)
        {
            if (montant <= 0) 
            { 
                return; 
                /*to do: remplacer par une erreur*/ 
            }
            //if(Solde - montant < - LigneDeCredit)
            //{ 
            //    return; 
            //    /*remplacer par une erreur*/
            //} //On peut aussi mettre ça dans le setter pour plus de réusabilité
            Solde = Solde - montant; 
        }
        public void Depot(double montant)
        {
            if (montant <= 0)
            {
                return; 
                /*to do: remplacer par une erreur*/ 
            }
            Solde = Solde + montant;
        }

    }
}

﻿
namespace GestionBanque_05.Classes
{
    public class Personne
    {
        public string Nom {  get; set; }
        public string Prenom { get; set; }

        public string NomComplet { 
            get
            {
                return Prenom + " " + Nom;
            }
        }
        public DateTime DateNaiss { get; set; }
    }
}

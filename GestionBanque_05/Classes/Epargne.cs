﻿
namespace GestionBanque_05.Classes
{
    // Créer une classe « Epargne » permettant la gestion d’un carnet d’épargne qui devra implémenter
    public class Epargne : Compte
    {
        // Champs

        // Les propriétés publiques :

        public DateTime DateDernierRetrait { get; set; }

        // Numéro(string)
        // Solde(double) - Lecture seule
        // DateDernierRetrait(DateTime) - représentant la date du dernier retrait sur le carnet
        // Titulaire(Personne)


        // Les méthodes publiques :
        // void Retrait(double Montant)
        // void Depot(double Montant)

        public override void Retrait(double montant)
        {
            base.Retrait(montant);
           
            DateDernierRetrait = DateTime.Now;
        }
       
    }
}

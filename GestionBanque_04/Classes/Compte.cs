﻿
namespace GestionBanque_04.Classes
{
    // Définir la classe « Compte » reprenant les parties commune aux classes « Courant » et « Epargne » en utilisant les concepts d’héritage, de redéfinition de méthodes et si besoin, de surcharge de méthodes et d’encapsulation.
    // Attention le niveau d’accessibilité du mutateur de la propriété Solde doit rester « private ».
    public class Compte
    {
        public Personne Titulaire { get; set; }
        private double _solde;
        private double _ligneDeCredit;
        public string Numero { get; set; }

        public virtual double LigneDeCredit
        {
            get
            {
                return _ligneDeCredit;
            }
            set
            {
                if (value >= 0)
                {
                    _ligneDeCredit = value;
                }
            }
        }

        public virtual double Solde
        {
            get
            {
                return _solde;
            }
            set
            {
                if (value >= -LigneDeCredit)
                {
                    _solde = value;
                }
            }
        }

        public static double operator +(double a, Compte b)
        {
            
            return a + ((b.Solde > 0) ? b.Solde : 0);
        }


        public virtual void Retrait(double montant)
        {
            if (montant <= 0)
            {
                return;
                /*to do: remplacer par une erreur*/
            }
            Solde = Solde - montant;
        }
        public void Depot(double montant)
        {
            if (montant <= 0)
            {
                return;
                /*to do: remplacer par une erreur*/
            }
            Solde = Solde + montant;
        }


    }
}

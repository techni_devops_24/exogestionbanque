﻿

namespace GestionBanque_04.Classes
{

    public class Banque
    {

        public string Nom {  get; set; }

        private Dictionary<string, Courant> _listeCompte = new Dictionary<string, Courant>();

        public Courant this[string Numero]
        {
            get
            {
                Courant compte;
                _listeCompte.TryGetValue(Numero, out compte);
                return compte;
            }

            set
            {
                _listeCompte[Numero] = value;
            }
        }

        public void Ajouter(Courant compte)
        {
            if(_listeCompte.ContainsKey(compte.Numero))
            {
                Console.WriteLine("Erreur, compte existant!");
                return;
                //to-do: remplacer par une erreur
            }
            
            _listeCompte.Add(compte.Numero, compte);
        }

        public void Supprimer(string Numero)
        {
            if(!_listeCompte.ContainsKey(Numero))
            {
                Console.WriteLine("Compte inexistant.");
                return;
                //to-do exception
            }

            _listeCompte.Remove(Numero);
            Console.WriteLine($"Compte {Numero} supprimé.");
        }

        // Ajouter une méthode « AvoirDesComptes » à la classe « Banque » recevant en paramètre le titulaire(Personne) qui calculera les avoirs de tous ses comptes en utilisant l’opérateur « + ».

        public double AvoirDesComptes(Personne personne)
        {
            double avoir = 0;

            foreach (Courant compte in _listeCompte.Values)
            {
                if(personne.Equals(compte.Titulaire))
                {
                    avoir += compte;
                }
            }
            return avoir;
        }


    }
}

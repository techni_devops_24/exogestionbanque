﻿

namespace GestionBanque_04.Classes
{
    public class Courant : Compte
    {
        private double _ligneDeCredit;        
        public override double LigneDeCredit {             
            get 
            { 
                return _ligneDeCredit; 
            } 
            set 
            {
                if (value >= 0) {
                    _ligneDeCredit = value;
                }
            } 
        }


        public override void Retrait(double montant)
        {
            if (Solde - montant >= -_ligneDeCredit)
            {
                base.Retrait(montant);
            }          
           
        }
        


    }
}

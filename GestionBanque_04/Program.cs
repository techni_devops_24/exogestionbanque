﻿using GestionBanque_04.Classes;
Console.OutputEncoding = System.Text.Encoding.UTF8;

Personne Bruno = new Personne();
Bruno.Nom = "Step";
Bruno.Prenom = "Bruno";
Bruno.DateNaiss = new DateTime(2000, 01, 01);

Courant Compte = new Courant();
Compte.Numero = "BE68 5390 0754 7034";
Compte.LigneDeCredit = 500;
Compte.Titulaire = Bruno;

Courant Compte2 = new Courant();
Compte2.Numero = "BE98 6847 3216 7998";
Compte2.LigneDeCredit = 500;
Compte2.Titulaire = Bruno;
Compte2.Depot(750);

Epargne Epargne1 = new Epargne();
Epargne1.Numero = "BE98 6847 4578 7998";
Epargne1.Titulaire = Bruno;
Epargne1.Depot(5_000_000);

bool continuer = true;
do
{
    Console.Clear();
    Console.WriteLine("Voici votre solde : " + Compte.Solde + " €");
    Console.WriteLine("Que souhaitez-vous faire ?");
    Console.WriteLine("1) Retirer");
    Console.WriteLine("2) Déposer");
    Console.WriteLine("3) Quitter");

    switch (Console.ReadLine())
    {
        case "1":
            Console.WriteLine("Veuillez entrer le montant à retirer ");
            if (double.TryParse(Console.ReadLine(), out double montantRetrait))
            {
                Compte.Retrait(montantRetrait);
            }
            break;

        case "2":
            Console.WriteLine("Veuillez entrer le montant à déposer ");

            if (double.TryParse(Console.ReadLine(), out double montantDepot))
            {
                Compte.Depot(montantDepot);
            }
            break;

        case "3":
            continuer = false;
            break;

        default:
            Console.WriteLine("Commande inconnue !");
            break;
    }

} while (continuer);

Banque belfius = new Banque();
belfius.Nom = "Belcul";

belfius.Ajouter(Compte);
Console.WriteLine($"La banque {belfius.Nom} a un compte appartenant à: {belfius[Compte.Numero].Titulaire.NomComplet}");
belfius.Ajouter(Compte);

belfius.Ajouter(Compte2);

Console.WriteLine($"L'avoir des comptes de {Bruno.NomComplet} est de {belfius.AvoirDesComptes(Bruno)} €");

//belfius.Supprimer(Compte.Numero);
//belfius.Supprimer(Compte.Numero);






﻿
namespace GestionBanque_07.Interfaces
{
    public interface ICustomer
    {
        double Solde {  get; }
        void Depot (double montant);
        void Retrait(double montant);
    }
}

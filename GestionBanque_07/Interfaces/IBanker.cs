﻿using GestionBanque_07.Classes;

namespace GestionBanque_07.Interfaces
{
    public interface IBanker : ICustomer
    {
        void AppliquerInteret();
        Personne Titulaire {  get; }
        string Numero { get; }
    }
}

﻿
namespace GestionBanque_07.Classes
{
    // Créer une classe « Epargne » permettant la gestion d’un carnet d’épargne qui devra implémenter
    public class Epargne : Compte
    {
        

        // Les propriétés publiques :

        public DateTime DateDernierRetrait { get; set; }

        public override void Retrait(double montant)
        {
            base.Retrait(montant);
           
            DateDernierRetrait = DateTime.Now;
        }

        protected override double CalculInteret()
        {
            return Solde * (4.5 / 100);
        }


    }
}

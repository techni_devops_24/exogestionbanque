﻿namespace GestionBanque_02.Classes
{
    //Créer une classe « Banque » qui gérera les comptes de la banque
    //Cette classe devra implémenter :
    public class Banque
    {
        // Les propriétés :
        
        // ◼ Nom(string) - Nom de la banque
        public string Nom {  get; set; }

        // Un indexeur retournant un compte sur base de son numéro.
        private Dictionary<string, Courant> _listeCompte = new Dictionary<string, Courant>();

        public Courant this[string Numero]
        {
            get
            {
                Courant compte;
                _listeCompte.TryGetValue(Numero, out compte);
                return compte;
            }

            set
            {
                _listeCompte[Numero] = value;
            }
        }

        // Les méthodes :
        // ◼ void Ajouter(Courant compte)
        public void Ajouter(Courant compte)
        {
            if(_listeCompte.ContainsKey(compte.Numero))
            {
                Console.WriteLine("Erreur, compte existant!");
                return;
                //to-do: remplacer par une erreur
            }
            
            _listeCompte.Add(compte.Numero, compte);
        }

        // ◼ void Supprimer(string Numero)
        public void Supprimer(string Numero)
        {
            if(!_listeCompte.ContainsKey(Numero))
            {
                Console.WriteLine("Compte inexistant.");
                return;
                //to-do exception
            }

            _listeCompte.Remove(Numero);
            Console.WriteLine($"Compte {Numero} supprimé.");
        }
    }
}

﻿
using System.Numerics;
using System.Reflection.Metadata;

namespace GestionBanque_03.Classes
{
    public class Courant
    {
        private double _ligneDeCredit;
        private double _solde;
        public string Numero {  get; set; }
        public double Solde { 
            get 
            {
                return _solde;
            } 
            private set 
            {
                if (value < -LigneDeCredit)
                {
                    return;
                    /*remplacer par une erreur*/
                }
                else
                {
                    _solde = value;
                }
            } 
        }
        public double LigneDeCredit {             
            get 
            { 
                return _ligneDeCredit; 
            } 
            set 
            {
                if (value >= 0) {
                    _ligneDeCredit = value;
                }
            } 
        }
        public Personne Titulaire { get; set; }


        
        // Surcharger l’opérateur « + » de la classe « Courant » afin qu’il retourne la somme, de type double, des soldes.
        // Cependant, les soldes négatifs ne doivent pas être pris en compte. :
        public static double operator +(double a, Courant b)
        {
            //if(b.Solde < 0)
            //{
            //    return a;
            //}            
            //return a + b.Solde;
            return a + ((b.Solde > 0) ? b.Solde : 0);
        }


        public void Retrait(double montant)
        {
            if (montant <= 0) 
            { 
                return; 
                /*to do: remplacer par une erreur*/ 
            }
            Solde = Solde - montant; 
        }
        public void Depot(double montant)
        {
            if (montant <= 0)
            {
                return; 
                /*to do: remplacer par une erreur*/ 
            }
            Solde = Solde + montant;
        }


    }
}

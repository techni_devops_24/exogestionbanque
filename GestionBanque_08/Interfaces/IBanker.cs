﻿using GestionBanque_08.Classes;

namespace GestionBanque_08.Interfaces
{
    public interface IBanker : ICustomer
    {
        void AppliquerInteret();
        Personne Titulaire {  get; }
        string Numero { get; }
    }
}

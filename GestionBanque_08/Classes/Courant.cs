﻿

using System.Reflection.Metadata;

namespace GestionBanque_08.Classes
{
    public class Courant : Compte
    {
        private double _ligneDeCredit;        
        public override double LigneDeCredit {             
            get 
            { 
                return _ligneDeCredit; 
            }
        }

        //Le cas échéant, ajoutez le ou les constructeurs aux classes « Courant » et « Epargne ».
        //Ajouter à la classe « Courant » un constructeur recevant en paramètre le numéro, le titulaire et la ligne de crédit.
        public Courant(string numero, Personne titulaire) : base(numero, titulaire)
        {
        
        }

        public Courant(string numero, Personne titulaire, double solde) : base(numero, titulaire, solde)
        {

        }

        public Courant(Personne titulaire, string numero, double ligneDeCredit, double solde = 0) : base(titulaire, numero, ligneDeCredit, solde)
        {

        }

        public override void Retrait(double montant)
        {
            if (Solde - montant >= -_ligneDeCredit)
            {
                base.Retrait(montant);
            }          
           
        }

        protected override double CalculInteret()
        {
            if(Solde >= 0)
            {
                return Solde * (3.0 / 100);
            }
            return Solde * (9.75 / 100);
        }

    }
}

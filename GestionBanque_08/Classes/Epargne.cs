﻿
namespace GestionBanque_08.Classes
{
    public class Epargne : Compte
    {
        public DateTime DateDernierRetrait { get; private set; }

        //Le cas échéant, ajoutez le ou les constructeurs aux classes « Courant » et « Epargne ».

        public Epargne(string numero, Personne titulaire) : base(numero, titulaire)
        {

        }

        public Epargne(string numero, Personne titulaire, double solde) : base(numero, titulaire, solde)
        {

        }

        public override void Retrait(double montant)
        {
            base.Retrait(montant);
           
            DateDernierRetrait = DateTime.Now;
        }

        protected override double CalculInteret()
        {
            return Solde * (4.5 / 100);
        }


    }
}

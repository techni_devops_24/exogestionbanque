﻿
namespace GestionBanque_08.Classes
{
    public class Personne
    {
        public string Nom {  get; private set; }
        public string Prenom { get; private set; }

        public string NomComplet { 
            get
            {
                return Prenom + " " + Nom;
            }
        }
        public DateTime DateNaiss { get; private set; }

        public Personne (string nom, string prenom, DateTime dateNaiss)
        {
            Nom = nom;
            Prenom = prenom;
            DateNaiss = dateNaiss;
        }
    }
}

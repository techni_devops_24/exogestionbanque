﻿
using GestionBanque_08.Interfaces;
using System.Net;
using System.Text;

namespace GestionBanque_08.Classes
{
    
    public abstract class Compte : IBanker
    {
        public Personne Titulaire { get; private set; }
        private double _solde;
        private double _ligneDeCredit;
        public string Numero { get; private set; }

        public virtual double LigneDeCredit
        {
            get
            {
                return _ligneDeCredit;
            }
            private set
            {
                if (value >= 0)
                {
                    _ligneDeCredit = value;
                }
            }
        }

        public virtual double Solde
        {
            get
            {
                return _solde;
            }
            private set
            {
                if (value >= -LigneDeCredit)
                {
                    _solde = value;
                }
            }
        }

        // 1. Ajoutez, dans la classe « Compte », deux constructeurs prenant en paramètre :
        // 1. Le numéro et le titulaire
        // 2. Le numéro, le titulaire et le solde(pour le cas d’une base de données)
        public Compte(string Numero, Personne titulaire)
        {
            this.Numero = Numero;
            Titulaire = titulaire;
        }

        public Compte(string Numero, Personne titulaire, double solde) : this(Numero, titulaire)
        {
            Solde = solde;
        }

        public Compte(Personne titulaire, string numero, double ligneDeCredit, double solde = 0) : this (numero, titulaire, solde)
        {
            LigneDeCredit = ligneDeCredit;
        }

        public static double operator +(double a, Compte b)
        {
            
            return a + ((b.Solde > 0) ? b.Solde : 0);
        }


        public virtual void Retrait(double montant)
        {
            if (montant <= 0)
            {
                return;
                /*to do: remplacer par une erreur*/
            }
            Solde = Solde - montant;
        }
        public void Depot(double montant)
        {
            if (montant <= 0)
            {
                return;
                /*to do: remplacer par une erreur*/
            }
            Solde = Solde + montant;
        }

        // Ajouter une méthode abstraite « protected » à la classe « Compte » appelée « CalculInteret » ayant pour prototype « double CalculInteret() » en sachant que pour un livret d’épargne le taux est toujours de 4.5% tandis que pour le compte courant si le solde est positif le taux sera de 3% sinon de 9.75%.
        protected abstract double CalculInteret();

        // 3. Ajouter une méthode « public » à la classe « Compte » appelée « AppliquerInteret » qui additionnera le solde avec le retour de la méthode « CalculInteret »
        public void AppliquerInteret()
        {
            double interet = CalculInteret();
            Solde += interet;
        }




    }
}
